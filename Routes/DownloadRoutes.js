const express = require("express");
const asyncHandler = require("express-async-handler");
const path = require("path");
const downloadRouter = express.Router();

downloadRouter.get(
  `/:fileName`,
  asyncHandler(async (req, res) => {
    const __dirname = path.dirname(__filename);
    
    const fileName = req.params?.fileName;
    const jsonPath = path.join(__dirname, "../uploads", fileName);
    try {
      res.download(jsonPath);
    } catch (err) {
      console.error(err);
    }
  })
);

module.exports = downloadRouter;
