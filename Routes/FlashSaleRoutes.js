const express = require("express");
const asyncHandler = require("express-async-handler");
const { admin, protect } = require("../Middleware/AuthMiddleware.js");
const FlashSale = require("./../Models/FlashSaleModel.js");

const flashsaleRoute = express.Router();

// Get all flash sale Admin
flashsaleRoute.get(
  "/",
  asyncHandler(async (req, res) => {
    const flashsales = await FlashSale.find({});
    res.json(flashsales);
  })
);

// Get all flash sale Admin
flashsaleRoute.get(
  "/all",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const flashsales = await FlashSale.find({});
    res.json(flashsales);
  })
);

// Get single flash sale
flashsaleRoute.get(
  "/:id",
  asyncHandler(async (req, res) => {
    const flashsale = await FlashSale.findById(req.params.id);
    if (flashsale) {
      res.json(flashsale);
    } else {
      res.status(404);
      throw new Error("Vente flash invalide");
    }
  })
);

// Create product
flashsaleRoute.post(
  "/",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const { name, image, description, price, countInStock } = req.body;
    const flashsaleExist = await FlashSale.findOne({ name });
    if (flashsaleExist) {
      res.status(404);
      throw new Error("Nom de vente flash déjà existe");
    } else {
      const flashsale = new FlashSale({
        name,
        image,
        description,
        price,
        countInStock,
        user: req.user._id,
      });
      if (flashsale) {
        const createdFlashsale = await flashsale.save();
        res.status(201).json(createdFlashsale);
      } else {
        res.status(400);
        throw new Error("Données invalide");
      }
    }
  })
);

// Edit flash sale
flashsaleRoute.put(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const { name, image, description, price, countInStock } = req.body;
    const flashsale = await FlashSale.findById(req.params.id);
    if (flashsale) {
      flashsale.name = name || flashsale.name;
      flashsale.image = image || flashsale.image;
      flashsale.description = description || flashsale.description;
      flashsale.price = price || flashsale.price;
      flashsale.countInStock = countInStock || flashsale.countInStock;

      const updatedFlashsale = await flashsale.save();
      res.json(updatedFlashsale);
    } else {
      res.status(404);
      throw new Error("Vente flash non trouvé");
    }
  })
);

// Delet flash sale
flashsaleRoute.delete(
  "/:id",
  protect,
  admin,
  asyncHandler(async (req, res) => {
    const flashsale = await FlashSale.findById(req.params.id);
    if (flashsale) {
      await flashsale.deleteOne();
      res.json({ message: "Vente flash supprimé" });
    } else {
      res.status(404);
      throw new Error("Produits invalide");
    }
  })
);

module.exports = flashsaleRoute;
