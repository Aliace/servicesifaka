const categories = [
  {
    name: "Works Hard",
    image: "https://preview.ibb.co/fuL4BS/learn_fast.jpg",
    description: "I don't give up until I've figured it out",
    slug: "Works Hard"
  },
  {
    name: "Awesome Skills",
    image: "https://preview.ibb.co/fuL4BS/learn_fast.jpg",
    description: "I don't give up until I've figured it out",
    slug: "Awesome Skills"
  },
  {
    name: "Fast Learner",
    image: "https://preview.ibb.co/fuL4BS/learn_fast.jpg",
    description: "I don't give up until I've figured it out",
    slug: "Fast Learner"
  },
];

module.exports = categories;
