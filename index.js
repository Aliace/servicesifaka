const express = require('express');
const products = require('./data/Products.js');
const dotenv = require('dotenv');
const connectDataBase = require('./config/MongoDb.js');
const ImportData = require("./DataImport.js");
const productRoute = require('./Routes/ProductRoutes.js');
const userRouter = require('./Routes/UserRoutes.js');
const orderRouter = require('./Routes/OrderRoutes.js');
const categoryRouter = require('./Routes/Category.js');
const flashsaleRoute = require('./Routes/FlashSaleRoutes.js');
const subCategoryRouter = require('./Routes/SubCategoryRoutes.js');
const imagesRouter = require('./Routes/ImageRoutes.js');
const downloadRouter = require('./Routes/DownloadRoutes.js');
const cors = require('cors');
const sifakaAnimationRouter = require('./Routes/SifakaAnimationRouter.js');
const flashSaleAnimationRouter = require('./Routes/FlashSaleAnimationRouter.js');

dotenv.config();
connectDataBase()
const app = express();
app.use(express.json());

const corsOptions = {
  origin: '*',
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

// API
app.use("/api/import", ImportData);
app.use('/api/products', productRoute);
app.use('/api/users', userRouter);
app.use('/api/orders', orderRouter);
app.use('/api/category', categoryRouter);
app.use('/api/subcategory', subCategoryRouter);
app.use('/api/venteflashs', flashsaleRoute);
app.use('/api/upload', imagesRouter);
app.use('/api/download', downloadRouter);
app.use('/api/animationFlashSale', flashSaleAnimationRouter);
app.use('/api/animationSifaka', sifakaAnimationRouter);


const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
    console.log(`Serveur démmare sur le port ${PORT}`)
})