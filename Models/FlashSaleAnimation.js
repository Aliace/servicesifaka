const mongoose = require("mongoose");

const flashSaleAnimationSchema = mongoose.Schema({
  animation: {
    type: String,
  },
});

const FlashSaleAnimation = mongoose.model(
  "FlashSaleAnimation",
  flashSaleAnimationSchema
);

module.exports = FlashSaleAnimation;
