const mongoose = require("mongoose");

const ImageSchema = mongoose.Schema({
  image: {
    data: Buffer,
    contentType: String,
  },
});

const UploadImage = mongoose.model("UploadImage", ImageSchema);

module.exports = UploadImage;
