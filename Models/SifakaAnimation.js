const mongoose = require("mongoose");

const sifakaAnimationSchema = mongoose.Schema({
  animation: {
    type: String,
  },
});

const SifakaAnimation = mongoose.model(
  "SifakaAnimation",
  sifakaAnimationSchema
);

module.exports = SifakaAnimation;
