const express = require("express");
const User = require("./Models/UserModel.js");
const users = require("./data/users.js");
const Product = require("./Models/ProductModel.js");
const products = require("./data/Products.js");
const asyncHandler = require("express-async-handler");
const Category = require("./Models/Category.js");
const categories = require("./data/Categories.js");
const FlashSale = require("./Models/FlashSaleModel.js");
const flashsales = require("./data/FlashSale.js");

const ImportData = express.Router();

ImportData.post(
  "/user",
  asyncHandler(async (req, res) => {
    await User.deleteOne({});
    const importUser = await User.insertMany(users);
    res.send({ importUser });
  })
);

ImportData.post(
  "/products",
  asyncHandler(async (req, res) => {
    await Product.deleteOne({});
    const importProducts = await Product.insertMany(products);
    res.send({ importProducts });
  })
);

ImportData.post(
  "/categories",
  asyncHandler(async (req, res) => {
    await Category.deleteOne({});
    const importCategorys = await Category.insertMany(categories);
    res.send({ importCategorys });
  })
);

ImportData.post(
  "/venteflashs",
  asyncHandler(async (req, res) => {
    await FlashSale.deleteOne({});
    const importFlashsales = await FlashSale.insertMany(flashsales);
    res.send({ importFlashsales });
  })
);

module.exports = ImportData;
