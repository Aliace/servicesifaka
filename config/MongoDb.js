const mongoose = require("mongoose");

const connectDataBase = async () => {
  try {
    const connect = await mongoose.connect(process.env.MONGO_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    console.log('Mongo connecté');
  } catch (err) {
    console.log(`Erreur: ${err.message}`);
    process.exit(1);
  }
};

module.exports = connectDataBase