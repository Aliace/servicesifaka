const jwt = require("jsonwebtoken");
const asyncHandler = require("express-async-handler");
const User = require("../Models/UserModel.js");

const protect = asyncHandler(async (req, res, next) => {
  let token;
  console.log("Auth", req.headers);
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    // console.log("Auth", req.headers.authorization);
    try {
      token = req.headers.authorization.split(" ")[1];

      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      const user = await User.findById(decoded.id).select("-password")
      if (!user) {
        res.status(401);
        throw new Error("Non autoriser, invalide token");
      }
      req.user = user;
      next();
    } catch (error) {
      console.error(error);
      res.status(401);
      throw new Error("Non autoriser, invalide token");
    }
  }
  if (!token) {
    res.status(401);
    throw new Error("Non autoriser");
  }
});

const admin = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next();
  } else {
    res.status(401);
    throw new Error("Non autoriser");
  }
};

module.exports = { protect, admin };
